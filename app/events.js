const router = require("express").Router();
const auth = require("../middleware/auth");
const Event = require("../models/Event");
const User = require('../models/User');

router.get("/", auth, async (req, res) => {
    try {
        const events = await Event.find().populate("user");
        res.send(events);
    } catch (e) {
        res.status(500).send(e);
    }
});
// router.get("/:id", async (req, res) => {
//     const result = await Product.findById(req.params.id);
//     if (result) {
//         res.send(result);
//     } else {
//         res.sendStatus(404);
//     }
// });
router.post("/", auth, async (req, res) => {
    const userId = req.user._id;
    // const userInfo = {...req.body}
    const userInfo = {...req.body, user: userId}
    let event = new Event(userInfo);
    try {
        await event.save();
        res.send(event);
    } catch(e) {
        console.log(e);
        res.status(400).send(e);
    }
});
router.post("/access", auth, async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    let query;
    if (req.query.user) {
         query = {user: req.query.user};
    }

    if (!user) {
        res.status(400).send(e);
    }
    const access = await Event.findOne(query);
    access.users = [...access.users, user]
    try {
        await access.save();
        res.send(access);
    } catch(e) {
        console.log(e);
        res.status(400).send(e);
    }
});

    // const productData = req.body;
//     const event = new Event(req.body);
//     try {
//         await event.save();
//         res.send(event);
//     } catch (e) {
//         res.status(400).send(e);
//     }
// });
router.delete("/:id", auth, async (req, res) => {
    try {
            const event = await Event.findByIdAndUpdate(req.params.id);
            await event.delete();
            res.send('Deleted');
    } catch(e) {
        res.sendStatus(400);
    }
});

module.exports = router;