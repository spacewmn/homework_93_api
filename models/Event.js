const mongoose = require("mongoose");
const idValidator = require("mongoose-id-validator");

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    users: [
        {
            type: Schema.Types.ObjectId,
            ref: "User"
        }
    ]
});

EventSchema.plugin(idValidator, {
    message: 'Bad ID value for {PATH}'
});

const Event = mongoose.model("Event", EventSchema);
module.exports = Event;