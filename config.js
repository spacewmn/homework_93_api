const path = require("path");

const rootPath = __dirname;

module.exports = {
    rootPath,
    db: {
        name: "calendar",
        url: "mongodb://localhost"
    },
    fb: {
        appId: "1095980987488580",
        appSecret: process.env.FB_SECRET
    }
};